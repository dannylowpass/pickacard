/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;
import java.util.Scanner;
/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
    public static void main(String[] args)
    {
        Card[] magicHand = new Card[7];
        int suitGen = 0;
        
        for (int i=0; i<magicHand.length; i++)
        {
            Card c = new Card();
            c.setValue(Math.random() *52);
            suitGen = Math.random() *3;
            switch(suitGen){
                case 1: 
                c.setSuit("Hearts");
                    break;
                case 2:
                c.setSuit("Diamonds");
                    break;
                case 3:
                c.setSuit("Spades");
                    break;
                case 4: 
                c.setSuit("Clubs");
                    break;
            }
        }
        
         System.out.println("What is your cards value (from 1-13, Ace = 1, "
                 + "King = 13: ");
         
        Card usersCard = new Card();
        Scanner in = new Scanner(System.in);
        usersCard.setValue(in.nextInt());
        
        System.out.println("Enter yours cards suit, with the first letter being"
                + " a capital");
        usersCard.setSuit(in.next());
        int equalSuit = usersCard.suit.compareTo(magicHand.suit);
        
        if (equalSuit != 0){
            System.out.println("Cards are not the same!");
        } else if (equalSuit == 0){
            int equalValue = usersCard.suit.compareTo(magicHand.suit);
        
            if (equalValue != 0){
                System.out.println("Cards are not the same!");
            }else if (equalValue == 0){
               System.out.println("Cards are the same!"); 
            }
        }
        
        
        
        
        
        
        
         
        
        
        //insert code to ask the user for Card value and suit, create their card
        // and search magicHand here
        //Then report the result here
    }
    
}
